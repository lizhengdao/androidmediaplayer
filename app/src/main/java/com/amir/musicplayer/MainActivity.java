package com.amir.musicplayer;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity {
    Button playAudio1;
    Button playAudio2;
    Button playAudio3;
    Button playAudio4;
    MediaPlayer mediaPlayer;
    SeekBar progress;
    SeekBar volume;
    ImageView playerPlay;
    ImageView playerPause;
    TextView songName;
    RelativeLayout playerPanel;
    int audioId;
    ImageView muteSound;
    ImageView muteSoundDisable;
    Boolean isZoomButton1 = false;
    Boolean isZoomButton2 = false;
    Boolean isZoomButton3 = false;
    Boolean isZoomButton4 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        playAudio1 = (Button) findViewById(R.id.playAudio1);
        playAudio2 = (Button) findViewById(R.id.playAudio2);
        playAudio3 = (Button) findViewById(R.id.playAudio3);
        playAudio4 = (Button) findViewById(R.id.playAudio4);
        playerPlay = (ImageView) findViewById(R.id.playerPlay);
        playerPause = (ImageView) findViewById(R.id.playerPause);
        songName = (TextView) findViewById(R.id.song_name);
        playerPanel = (RelativeLayout) findViewById(R.id.playerPanel);
        muteSound = (ImageView) findViewById(R.id.noVolume);
        muteSoundDisable = (ImageView) findViewById(R.id.maxVolume);

        mediaPlayer = null;
        progress = (SeekBar) findViewById(R.id.progressControl);
        volume = (SeekBar) findViewById(R.id.volume);
        playerPanel.setTranslationY(500f);

        final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int max = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int curr = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        volume.setMax(max);
        volume.setProgress(curr);
        changeIcon();


        volume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, i, 0);
                Log.i("Info", "Mijenjam jacinu zvuka");
                changeIcon();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        progress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                //mediaPlayer.seekTo(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }


    public void playAudio1(View view) {
        resetPlayer();
        audioId = R.raw.audio_one;
        songName.setText("A Boogie Wit Da Hoodie");

        if (isZoomButton1 == false) {
            resetPlayer();
            playAudio();
            runTimer();
            // playAudio1.startAnimation(animationZoom);
            isZoomButton1 = true;
            playerPanel.animate().translationYBy(-500f).setDuration(1000);

            playAudio1.setScaleX(1.3f);
            playAudio1.setScaleY(1.3f);

            playAudio2.setEnabled(false);
            playAudio3.setEnabled(false);
            playAudio4.setEnabled(false);
        } else {
            resetPlayer();
            playerPanel.animate().translationYBy(500f).setDuration(1000);
            isZoomButton1 = false;

            playAudio1.setScaleX(1f);
            playAudio1.setScaleY(1f);

            playAudio2.setEnabled(true);
            playAudio3.setEnabled(true);
            playAudio4.setEnabled(true);
        }


    }


    public void playAudio2(View view) {
        resetPlayer();
        audioId = R.raw.pitbull;
        songName.setText("Pitbull - Rain Over Me");
        if (isZoomButton2 == false) {
            playAudio();
            runTimer();

            playAudio2.setScaleX(1.3f);
            playAudio2.setScaleY(1.3f);

            isZoomButton2 = true;
            playerPanel.animate().translationYBy(-500f).setDuration(1000);

            playAudio1.setEnabled(false);
            playAudio3.setEnabled(false);
            playAudio4.setEnabled(false);
        } else {
            playerPanel.animate().translationYBy(500f).setDuration(1000);
            isZoomButton2 = false;

            playAudio2.setScaleX(1f);
            playAudio2.setScaleY(1f);

            playAudio1.setEnabled(true);
            playAudio3.setEnabled(true);
            playAudio4.setEnabled(true);
        }
    }


    public void playAudio3(View view) {
        resetPlayer();
        audioId = R.raw.gangnam;
        songName.setText("PSY - GANGNAM STYLE");
        if (isZoomButton3 == false) {
            playAudio();
            runTimer();

            playAudio3.setScaleX(1.3f);
            playAudio3.setScaleY(1.3f);

            isZoomButton3 = true;
            playerPanel.animate().translationYBy(-500f).setDuration(1000);

            playAudio1.setEnabled(false);
            playAudio2.setEnabled(false);
            playAudio4.setEnabled(false);
        } else {
            playerPanel.animate().translationYBy(500f).setDuration(1000);
            isZoomButton3 = false;


            playAudio3.setScaleX(1f);
            playAudio3.setScaleY(1f);

            playAudio1.setEnabled(true);
            playAudio2.setEnabled(true);
            playAudio4.setEnabled(true);
        }

    }

    public void playAudio4(View view) {
        resetPlayer();
        audioId = R.raw.crazy;
        songName.setText("Crazy Frog - Axel F");
        if (isZoomButton4 == false) {
            playAudio();
            runTimer();
            playAudio4.setScaleX(1.3f);
            playAudio4.setScaleY(1.3f);

            isZoomButton4 = true;
            playerPanel.animate().translationYBy(-500f).setDuration(1000);

            playAudio1.setEnabled(false);
            playAudio3.setEnabled(false);
            playAudio2.setEnabled(false);
        } else {
            playerPanel.animate().translationYBy(500f).setDuration(1000);
            isZoomButton4 = false;


            playAudio4.setScaleX(1f);
            playAudio4.setScaleY(1f);

            playAudio1.setEnabled(true);
            playAudio3.setEnabled(true);
            playAudio2.setEnabled(true);
        }

    }

    public void startPlayer(View view) {
        if (mediaPlayer == null) {
            Toast.makeText(this, "Please select the song.", Toast.LENGTH_SHORT).show();
        } else {
            mediaPlayer.start();
            playerPlay.setVisibility(View.GONE);
            playerPause.setVisibility(View.VISIBLE);


        }


    }


    public void pausePlayer(View view) {
        mediaPlayer.stop();
        playerPause.setVisibility(View.GONE);
        playerPlay.setVisibility(View.VISIBLE);
        progress.setProgress(0);
        mediaPlayer = MediaPlayer.create(this, audioId);

    }


    public void playAudio() {
        // zaustavi audio koji svira
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
            progress.setProgress(0);
        }
        else {
            mediaPlayer = MediaPlayer.create(this, audioId);

        }

    }

    public void setVolumeNoSound(View view) {
        volume.setProgress(10);
    }

    public void setVolumeMaxSound(View view) {
        volume.setProgress(0);
    }

    public void resetPlayer() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            playerPause.setVisibility(View.GONE);
            playerPlay.setVisibility(View.VISIBLE);
            progress.setProgress(0);
        }

    }


    public void changeIcon() {
        if (volume.getProgress() == 0) {
            muteSoundDisable.setVisibility(View.GONE);
            muteSound.setVisibility(View.VISIBLE);
        } else {
            muteSoundDisable.setVisibility(View.VISIBLE);
            muteSound.setVisibility(View.GONE);
        }
    }


    public void runTimer() {
        progress.setMax(mediaPlayer.getDuration());
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                progress.setProgress(mediaPlayer.getCurrentPosition());
                Log.i("Info", "Trenutno sam na poziciji; " + mediaPlayer.getCurrentPosition() / 1000);
            }
        }, 0, 1000);
    }


}
